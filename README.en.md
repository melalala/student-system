# SSM+LayUi学生学籍管理系统

#### Description
基于SSM+LayUi实现的学籍管理系统

运行环境
jdk7/8+mysql5.6+IntelliJ IDEA+Navicat

技术点
ssm+layui+jsp+echarts

功能点
本系统具有三种用户：
1.管理员：专业管理，班级管理，学生管理，老师管理，课程管理，开课管理以及用户管理
2.教师：成绩管理，学生查询
3.学生：选课管理，查看成绩


#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
