# SSM+LayUi学生学籍管理系统

#### 介绍
基于SSM+LayUi实现的学籍管理系统

运行环境
jdk7/8+mysql5.6+IntelliJ IDEA+Navicat

技术点
ssm+layui+jsp+echarts

功能点
本系统具有三种用户：
1.管理员：专业管理，班级管理，学生管理，老师管理，课程管理，开课管理以及用户管理
2.教师：成绩管理，学生查询
3.学生：选课管理，查看成绩


#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
